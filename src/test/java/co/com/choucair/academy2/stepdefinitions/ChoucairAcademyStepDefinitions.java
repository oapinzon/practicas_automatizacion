package co.com.choucair.academy2.stepdefinitions;

import co.com.choucair.academy2.model.DataDriven;
import co.com.choucair.academy2.questions.Answer;
import co.com.choucair.academy2.tasks.Login;
import co.com.choucair.academy2.tasks.OpenUp;
import co.com.choucair.academy2.tasks.Search;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;

import java.util.List;

public class ChoucairAcademyStepDefinitions {
    @Before
    public void setStage(){
        OnStage.setTheStage(new OnlineCast());
    }

    @Given("^that Omar wants to learn automation at the academy choucair\\.$")
    public void thatOmarWantsToLearnAutomationAtTheAcademyChoucair(List<DataDriven> course1){
        OnStage.theActorCalled("Omar").wasAbleTo(OpenUp.thePage(), (Login.OnThePage(course1)));
    }

    @When("^he searh for the course on the choucair academy plataform$")
    public void heSearhForTheCourseAutomatizaciónDePruebasOnTheChoucairAcademyPlataform(List<DataDriven> course){
        OnStage.theActorInTheSpotlight().attemptsTo(Search.the(course));
    }

    @Then("^he finds the course called (.*)$")
    public void heFindsTheCourseCalledAutomatizaciónDePruebas(String question){
        OnStage.theActorInTheSpotlight().should(GivenWhenThen.seeThat(Answer.toThe(question)));
    }


}

package co.com.choucair.academy2.tasks;

import co.com.choucair.academy2.model.DataDriven;
import co.com.choucair.academy2.userinterface.SearchCoursePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import java.util.List;

public class Search implements Task {

    private List<DataDriven> course;

    public Search(List<DataDriven> course) {
        this.course = course;
    }

    public static Search the(List<DataDriven> course) {
        return Tasks.instrumented(Search.class,course);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Click.on(SearchCoursePage.UNIVERSIDAD_CHOUCAIR),
                Enter.theValue(course.get(0).getBuscarCursos()).into(SearchCoursePage.INPUT_THE_COURSE),
                Click.on(SearchCoursePage.BUTTON_GO),
                Click.on(SearchCoursePage.SELECT_COURSE)
        );
    }
}

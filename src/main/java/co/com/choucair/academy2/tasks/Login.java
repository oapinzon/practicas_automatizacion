package co.com.choucair.academy2.tasks;

import co.com.choucair.academy2.model.DataDriven;
import co.com.choucair.academy2.userinterface.ChoucairLoginPage;
import cucumber.api.java.vi.Cho;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.EnterValue;

import java.util.List;

public class Login implements Task {
    private List<DataDriven> course1;

    public Login(List<DataDriven> course1) {
        this.course1 = course1;
    }

    public static Login OnThePage(List<DataDriven> course1) {
        return Tasks.instrumented(Login.class, course1);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Click.on(ChoucairLoginPage.LOGIN_BUTTON),
                Enter.theValue(course1.get(0).getUserName()).into(ChoucairLoginPage.INPUT_USER),
                Enter.theValue(course1.get(0).getPassword()).into(ChoucairLoginPage.INPUT_PASSWORD),
                Click.on(ChoucairLoginPage.ENTER_BUTTON)
        );
    }
}

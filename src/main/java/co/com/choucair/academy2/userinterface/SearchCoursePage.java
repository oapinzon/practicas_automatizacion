package co.com.choucair.academy2.userinterface;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import org.apache.tools.ant.taskdefs.Tar;
import org.openqa.selenium.By;

public class SearchCoursePage extends PageObject {
    public static final Target UNIVERSIDAD_CHOUCAIR = Target.the("Select Universidad Choucair").located(By.xpath("//div[@id='universidad']//strong"));
    public static final Target INPUT_THE_COURSE =Target.the("Where do we put the name of the course").located(By.id("coursesearchbox"));
    public static final Target BUTTON_GO = Target.the("Button to begin the search").located(By.xpath("//button[@class='btn btn-secondary']"));
    public static final Target SELECT_COURSE = Target.the("Select the course finded").located(By.xpath("//h4[contains(text(), 'Automatización de Pruebas')]"));
    public static final Target NAME_COURSE = Target.the("Get the name of the course").located(By.xpath("//h1[contains(text(), 'Automatización de Pruebas')]"));
}

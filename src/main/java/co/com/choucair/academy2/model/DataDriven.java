package co.com.choucair.academy2.model;

public class DataDriven {
    private String userName;
    private String password;
    private String buscarCursos;

    public String getUserName(){
        return userName;
    }

    public String getPassword(){
        return password;
    }

    public String getBuscarCursos(){
        return buscarCursos;
    }
}
